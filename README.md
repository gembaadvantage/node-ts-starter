# TypeScript starter

A minimal-ish TypeScript starter, using [tsup](https://tsup.egoist.sh/).

## Getting started

Install dependencies with

`npm install`

and then run

`npm run start:watch`

to start the app in watch mode, i.e. with hot-reloading.\
Unit tests can be run using

`npm test`
