import { add } from "./math/example";

const sum = add(1, 5);

console.log(`1 + 5 = ${sum}, hopefully.`);
