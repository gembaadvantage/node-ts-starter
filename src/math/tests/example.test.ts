import { add, sub } from "../example";

describe("Add", () => {
  test("Adds things", () => {
    expect(add(1, 3)).toEqual(4);
  });
});

describe("Sub", () => {
  test("Subtracts things", () => {
    expect(sub(1, 3)).toEqual(-2);
  });
});
