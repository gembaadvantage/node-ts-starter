/**
 * Adds two numbers
 *
 * @param a A
 * @param b B
 * @returns The sum of a and b
 */
export function add(a: number, b: number): number {
  return a + b;
}

/**
 * Finds the difference between two numbers
 *
 * @param a A
 * @param b B
 * @returns A sub B
 */
export function sub(a: number, b: number): number {
  return a - b;
}
